
## Init
```git
git init
```

## Remot connection
```git
git remote add origin https://ffprat:VjyLA4SXXkHLbdMFauQW@bitbucket.org/ffprat/{repo_name}.git
git push -u origin master  # only first time, else git push
```

## git clone
Clone command wil create the repo_folder
```git
git clone https://ffprat:VjyLA4SXXkHLbdMFauQW@bitbucket.org/ffprat/GridTrading.git
```

## Removed cached files
```git
git rm -r --cached .
```

## Status
```git
git status
git remote --help
git remote -v
git remote show
git remote show origin
type .git/HEAD
git branch -a  # see all branches
```

## Create local branch
```git
git checkout -b newbranch  # creates a new branch, I can git pull to this new branch
git checkout master  # go back to master branch
git merge newbranch  # merges the newbranch with master (option --no-ff)
git branch -d newbranch  # deletes the branch
```

## Restore from commit
Check one branch
```git
git branch branch_name <commit_hash>  # or HEAD~3  (0ace6e9)
```

Create a new brach with an old commit
```git
git checkout -b branch_name <commit_hash>  # creates a new branch
git checkout <commit_hash>  # only to check
```

## merge a branch into master
Without conflicts
```git
git checkout master
git merge newbranch  # no conflict
git merge --abort
```

With conflicts
```git
git checkout master
git merge -X theirs final_branch
```

With conflicts (old)
```git
git checkout final_branch
git merge -s ours master  # ours == HEAD == current checkout
git checkout master
git merge final_branch
```

## Pull
With conflicts
```git
git fetch origin
git merge -X theirs origin/master
```

Force
```git
git fetch origin
git reset –hard master/origin
```

## Restore a file from local repo to working directory
```git
git restore <file>
git rm ????
```