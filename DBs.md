
# Services
[List of all webdev services](https://archive.org/details/github.com-ripienaar-free-for-dev_-_2022-01-19_14-07-57)

# Database
## SQL DBs
The correct way to pass variables in a SQL command is using the second argument of the execute() method!

### sqlit3
python library: [sqlite3](https://docs.python.org/3/library/sqlite3.html#sqlite3-placeholders)
	
Named arguments: :var0, dict  
[Exceptions](https://docs.python.org/3/library/sqlite3.html#exceptions)


### PostgreSQL
python library: [psycopg2](https://www.psycopg.org/docs/usage.html#query-parameters)

Named arguments: %(var0)s, dict  
[with statement](https://www.psycopg.org/docs/usage.html#with-statement)  
Context manager (with), commits but doesn't close

### SQLalchemy 
Python framework for all DBs

[Connect](https://docs.sqlalchemy.org/en/14/core/engines.html#backend-specific-urls)  
[Tutorial](https://docs.sqlalchemy.org/en/14/tutorial/dbapi_transactions.html#getting-a-connection)  
Named arguments: :var0, dict  
Context manager (with), closes but doesn't commit  


### mongoDB
NoSQL (stores data in JSON-like documents)

https://www.mongodb.com/languages/python
python library: pymongo


## DB software as a service (DBaaS)
[bit.io](https://bit.io/pricing)  
[ElephantSQL (Postgresql)](https://www.elephantsql.com/plans.html)  
[Heroku Postgres](https://www.heroku.com/postgres)  
[fly.io](https://fly.io/docs/reference/postgres/)  
[PlanetScale (MySQL)](https://planetscale.com/)  
[YugabyteDB](https://cloud.yugabyte.com/login)  

### Paid
* Amazon RDS (relational BD service), 12 months free: https://aws.amazon.com/rds/
* Amazon S3 (object storage)
* Azure (Microsoft), 12 months free: https://azure.microsoft.com/en-gb/free/, https://docs.microsoft.com/en-us/azure/azure-sql/database/free-sql-db-free-account-how-to-deploy?view=azuresql

## Servers to Host your apps in the cloud (PaaS) - Platform as a Service
Heroku  
[Fly](https://fly.io/docs/getting-started/python/)  

## Authentification
[Auth0](https://auth0.com/docs)  

## Create table
[sqlite](https://www.sqlitetutorial.net/sqlite-create-table/)  
[postgreslq](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-create-table/)  

SQL supports PRIMARY KEY, UNIQUE, NOT NULL, and CHECK column constraints.    
Data types sqlite: NULL, INTEGER, REAL, TEXT  
Data types postgrersql: INTEGER, VARCHAR(), bool, float(), numeric, numeric(), ...  

"""sql
DROP TABLE IF EXISTS customers;

CREATE TABLE customers (
	id INTEGER PRIMARY KEY AUTOINCREMENT, (sqlite)
	id serial PRIMARY KEY, (postgresql)
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR UNIQUE,
	email VARCHAR NOT NULL,
	active bool NOT NULL DEFAULT TRUE,
	PRIMARY KEY (id, name),
   	FOREIGN KEY (id) 
      		REFERENCES contacts (contact_id) 
         	ON DELETE CASCADE 
         	ON UPDATE NO ACTION
);
"""