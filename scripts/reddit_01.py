"""

Links:
    https://www.youtube.com/watch?v=CJAdCLZaISw
    https://github.com/hackingthemarkets/wallstreetbets-tracker/blob/main/search_wsb.py
    
Library:
    https://psaw.readthedocs.io/en/latest/
    https://melaniewalsh.github.io/Intro-Cultural-Analytics/04-Data-Collection/14-Reddit-Data.html
    
"""
from psaw import PushshiftAPI
import datetime
import sqlalchemy
import pandas as pd



f_path = "C:/frodriguezp/DataScience/Investing"
#os.chdir(f_path + "/Scripts")

engine = sqlalchemy.create_engine('sqlite:///' + f_path + '/data/stocks.db')
stocks = pd.read_sql('SELECT name FROM sqlite_master WHERE type="table"', engine).name.to_list()

#stocks = {}
#for row in rows: 
#    stocks['$' + row['symbol']] = row['id']
    

api = PushshiftAPI()

start_time = int(datetime.datetime(2022, 1, 1).timestamp())

submissions = api.search_submissions(after=start_time,
                                     subreddit='wallstreetsilver',
                                     filter=['url', 'author', 'title', 'subreddit'],
                                     )

mentions = []

for submission in submissions:
    words = submission.title.split()
    cashtags = [word.lower() for word in words if word.lower().startswith('$')]
#    cashtags = [word.lower() for word in words if word.lower()=='silver']

    if len(cashtags) > 0:
        submitted_time = datetime.datetime.fromtimestamp(submission.created_utc).isoformat()
#        print(submitted_time, cashtags)
#        print(submission.title) 
        for cashtag in cashtags:
            if 1: # cashtag.replace('$','') in stocks
                mentions.append([cashtag, submitted_time, submission.title, submission.url])
    
df = pd.DataFrame(mentions, columns=['cashtag','time','title','url'])
        

