

"""
  https://www.tutorialspoint.com/python/python_exceptions.htm
"""


import time
import math
from functools import wraps


## Decorator to retry a funcion
def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    """Retry calling the decorated function using an exponential backoff.
    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    """
    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = "  Error: retrying in {:.1f} seconds...".format(mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)
        return f_retry  # true decorator
    return deco_retry


class custom_error(Exception):
    pass


@retry(Exception, tries=4, delay=1, backoff=1.5)
def test_fail(x):
    raise Exception ('test exception')
    raise custom_error(55,'aaa',44)
    a=[1,2,3]
    a[5]
    return x**2

try:
    aa = test_fail(5)
    print(aa)
except Exception as e:
    print('Unexpected error: {} ({})'.format(e.__class__.__name__, e.args[0]))
    print(repr(e))
    

print('Finish')




