# -*- coding: utf-8 -*-
"""
Created on Wed Dec 22 11:46:24 2021

@author: ESRodrigFr1

https://www.youtube.com/watch?v=2IW-ZEui4h4
https://www.youtube.com/watch?v=nFn4_nA_yk8
pokemon code: https://www.twilio.com/blog/asynchronous-http-requests-in-python-with-aiohttp
official: https://docs.python.org/3/library/asyncio-task.html#creating-tasks
"""

import aiohttp
import asyncio
import time
import pandas as pd
import os

## Path: tiene que contener el codigo en scripts y la matrir hist_f en data
# f_path = "/Users/Francisco/Desktop/Tennis_table/"
# f_path = "C:/Users/WillyWonka/Documents/PingPong/"
f_path = "C:/frodriguezp/DataScience/Sports Betting/"
os.chdir(f_path + "scripts/")


hist1 = pd.read_feather(f_path + 'data/hist_f.feather')
upcoming3 = hist1.tail(50)

start_time = time.time()

    
# @retry(Exception, tries=3, delay=3, backoff=1.5)
async def get_upcoming_odds(session, event_id):
    url_base = 'https://api.b365api.com/v2/event/odds/summary?token={}&event_id={}'
    print('Getting odds for event:', event_id)
    try:
        async with session.get(url_base.format(api_token, event_id), ssl=False) as response:
            web = await response.json()
            cc = np.array([
                event_id,
                web['results']['Bet365']['odds']['end']['92_1']['home_od'],
                web['results']['Bet365']['odds']['end']['92_1']['away_od']
                ])
            return cc
    except: return None #pass


async def call_upcoming_odds(upcoming3):
    
    ## Async API calls
    async with aiohttp.ClientSession() as session:
        tasks = []
        for ii in range(upcoming3.shape[0]):
            event_id = upcoming3.event_id.iloc[ii]
            tasks.append(asyncio.create_task(get_upcoming_odds(session, event_id))) # In Python 3.7+
        responses = await asyncio.gather(*tasks)
    
    ## Stack all odds
    odds1 = np.empty((0,3), str)
    for response in responses:
        if response: odds1 = np.vstack((odds1,response))
      
    ## DataFrame
    odds_f = pd.DataFrame(odds1)
    odds_f.columns = ['event_id','cuota1','cuota2']
    odds_f['cuota1']=pd.to_numeric(odds_f['cuota1'], errors='coerce')
    odds_f['cuota2']=pd.to_numeric(odds_f['cuota2'], errors='coerce')
    
    ## Join Upcoming + Odds
    upcoming_f = upcoming3.merge(odds_f, on=["event_id"], how="inner")
    upcoming_f.reset_index(drop=True, inplace=True)
    
    ## New variables
    upcoming_f['date_exec'] = datetime.now()
    upcoming_f['d_sparks']=0
    upcoming_f['b_features']=0
    upcoming_f['S_rank']=0
    upcoming_f['x1_rank']=0
    upcoming_f['S_hh']=0
    upcoming_f['x4_hh']=0
    upcoming_f['S_co']=0
    upcoming_f['x4_co']=0
    upcoming_f['S_racha']=0
    upcoming_f['x4_racha']=0
    upcoming_f['x5_racha']=0
    upcoming_f['n_matches_p1']=0
    upcoming_f['n_matches_p2']=0
    upcoming_f['n_matches_setka_p1']=0
    upcoming_f['n_matches_setka_p2']=0
    upcoming_f['delta_rank']=0
    upcoming_f['rank_p1']=0
    upcoming_f['rank_p2']=0
    upcoming_f['rank_setka_p1']=0
    upcoming_f['rank_setka_p2']=0
    
    return upcoming_f
    

upcoming_f =  asyncio.run(call_upcoming_odds(upcoming3))
print("--- %s seconds ---" % (time.time() - start_time))



