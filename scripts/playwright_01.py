# -*- coding: utf-8 -*-
"""

CMD: playwright codegen
Actions:
    page.mouse.wheel(x, y)
    
    
CSS selectors
    id: #id
    class: .class1.class2
    div: div[property=value]
Javascript console:
    document.querySelector(".ovm-CompetitionHeader_NameText")
    document.querySelectorAll(".gl-Market.gl-Market_General.gl-Market_General-columnheader")

Links
    https://playwright.dev/python/docs/intro
    
"""

import time

# from bs4 import BeautifulSoup
from playwright.sync_api import sync_playwright

event_ii = 10
player_win = 2  # 1 o 2

with sync_playwright() as p:
    browser = p.chromium.launch(headless=False)
    page = browser.new_page()
    page.goto("https://www.bet365.de/#/IP/B92")
    print(page.title())
    
    # time.sleep(5)
    
    # Select. query_selector vs locator
    # locators = page.locator(".ovm-CompetitionHeader_NameText")  # League names
    locators = page.locator(".ovm-FixtureDetailsWithIndicators_Wrapper")  # Events
    
    # Wait
    locators.nth(0).hover()
    n_events = locators.count()
    print(n_events)
    
    try:
        locators.nth(n_events).hover(timeout=3000)
    except:
        time.sleep(2)
    n_events = locators.count()
    print(n_events)
    
    # Inner text
    events = locators.all_inner_texts()
    for ii, event in enumerate(events):
        event_data = event.split('\n')
        
        if ii == event_ii:
            print('Partido encontrado')
            locators.nth(ii).click()
            url = page.url
            print(url)
            print(event_data)
            
            # Odds
            # 1gl-Market gl-Market_General gl-Market_General-columnheader
            columns = page.locator(".gl-Market.gl-Market_General.gl-Market_General-columnheader")
            columns.nth(0).hover()
            n_cols = columns.count()
            print(n_cols)
            
            # Column 0: innerText: " \nGanador\nHándicap - Juegos\nHándicap (puntos)\nTotal"
            col = columns.nth(player_win).inner_text()
            # print(columns.nth(player_win).inner_html())
            col_data = col.split('\n')
            if len(col_data)==8:
                col_data = [col_data[0],col_data[1],col_data[6],col_data[7]]
            if len(col_data)==6:
                col_data = [col_data[0],col_data[1],col_data[4],col_data[5]]
            elif len(col_data)==4:
                pass
            elif len(col_data)==2:
                col_data = [col_data[0],col_data[1],'-','-']
                
            print(col_data)
            
            break
        
    time.sleep(5)
    
    # html = locator.inner_html()
    # print(html)

    # CLose
    browser.close()
    
    
# with https://www.bet365.de/#/IP/B92
# Access denied