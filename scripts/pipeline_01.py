# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 14:16:10 2022

@author: ESRodrigFr1

ArjanCodes
  https://github.com/ArjanCodes/2021-data-science-refactor
  https://www.youtube.com/watch?v=ka70COItN40

See also pipe in Pandas:
  https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.pipe.html
  https://www.geeksforgeeks.org/create-a-pipeline-in-pandas/
  
Scripts for TennisTable_v2
  tennis_table_v2
  API_calls
  running
  bet365_driver
  
"""

import functools
from typing import Callable
import pandas as pd

ComposableFunction = Callable[[float], float]

# Hyperparameters
MULTIPLIER = 3

# Helper function for composing functions
def compose(*functions: ComposableFunction) -> ComposableFunction:
    return functools.reduce(lambda f, g: lambda x: g(f(x)), functions)


def addThree(x: float) -> float:
    return x + 3


def multiplyByTwo(x: float) -> float:
    return x * 2


def addN(n: float) -> ComposableFunction:
    return lambda x: x + n


def create_df():
    df = pd.DataFrame({
        'id': [1,2,3,4,5,6,7,8,9],
        'var1': [1,2,3,4,5,6,7,8,9]
        })
    return df
    
def filter_df(df):
    df = df.copy()
    df = df[df.id<5]
    return df

def add_cols(df, multiplier):
    df = df.copy()
    df['new1'] = multiplier*df.var1
    return df




def main():
    x = 12
    # oldres = multiplyByTwo(multiplyByTwo(addThree(addThree(x))))
    myfunc = compose(addN(3), addN(3), multiplyByTwo, multiplyByTwo)
    result = myfunc(x)
    print(f"Result: {result}")

def main_df():
    df = create_df()
    df = (df
        .pipe(filter_df)
        .pipe(add_cols, multiplier=MULTIPLIER)
    ) 
    return df

    
    
if __name__ == "__main__":
    df = main_df()
    print(df)