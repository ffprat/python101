# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 15:55:48 2022

@author: ESRodrigFr1

SQLAlchemy
    https://towardsdatascience.com/sqlalchemy-python-tutorial-79a577141a91
    https://docs.sqlalchemy.org/en/14/core/engines.html#sqlite
    https://docs.sqlalchemy.org/en/14/core/connections.html

SQLite3
    pip install sqlite3
    https://www.sqlitetutorial.net/
    https://docs.python.org/3/library/sqlite3.html
    
    GUI tool:
        https://sqlitebrowser.org/dl/
        SQLiteStudio, DBeaver, DB Browser

MySQL
    pip install mysql-connector-python
    https://www.mysqltutorial.org/
    
    GUI tool:
        MySQL Workbench
        phpMyAdmin
        HeidiSQL (free edition of SQL Workbench)
    
    
PostgreSQL
    pip install psycopg2
    https://www.postgresqltutorial.com/
    https://www.youtube.com/watch?v=v-VWgQ-O540
    https://github.com/aspromatis/Stock-Database
    https://www.postgresqltutorial.com/postgresql-python/connect/
    https://www.postgresqltutorial.com/psql-commands/
    
TimescaleDB = PostgreSQL + time-series
    https://github.com/hackingthemarkets/timescaledb-aiohttp-asyncpg
    https://www.youtube.com/watch?v=MFudksxlZjk&list=PLvzuUVysUFOsrxL7UxmMrVqS8X2X0b8jd
    https://www.timescale.com/
    https://www.docker.com/get-started
    https://docs.timescale.com/install/latest/installation-docker/#install-self-hosted-timescaledb-from-a-pre-built-container
    cmd: docker images; docker ps; docker exec -it timescaledb bash; psql -U postgres
    cmd: docker exec -it timescaledb psql -U postgres
    
    GUI management tool
        TablePlus: https://tableplus.com/
        PgAdmin: https://www.pgadmin.org/
        https://www.jetbrains.com/datagrip/
        https://dbschema.com/
        https://dbeaver.io/, https://www.heidisql.com/

API requests: https://insomnia.rest/
"""

import sqlalchemy
from sqlalchemy import text
import sqlite3

import pandas as pd
import os


# =============================================================================
# SQLAlchemy
# =============================================================================

# *.db is stored in cd ~
engine = sqlalchemy.create_engine('sqlite:///TennisTable.db')

f_path = "C:/frodriguezp/DataScience/Sports Betting/"
md1 = pd.read_feather(f_path + 'data/hist_f.feather')

md1.to_sql('hist_f', engine, if_exists='append', index=False) # if_exists={‘fail’, ‘replace’, ‘append’}
# md1.to_csv(path, mode='a', header=False, index=False)

tables = pd.read_sql('SELECT name FROM sqlite_master WHERE type="table"', engine).name.to_list()
#bb1 = pd.read_sql('hist1', engine)
player_id = '231482'
bb2 = pd.read_sql(f"""SELECT * FROM hist_f WHERE player1_id = {player_id}""", engine)



## FInance
f_path = "C:/frodriguezp/DataScience/Investing"
os.chdir(f_path + "/Scripts")

engine = sqlalchemy.create_engine('sqlite:///' + f_path + '/data/stocks.db')
tables = pd.read_sql('SELECT name FROM sqlite_master WHERE type="table"', engine).name.to_list()


ticker = 'stocks'
df = pd.read_sql(ticker, engine)

with engine.connect() as connection:
    query = text("""delete from 'SILJ'
                 where date='2022-01-14 00:00:00.000000'
                 """)
    result = connection.execute(query)

#    for ii,row in enumerate(result):
#        if ii>4995:
#            print(ii, row)
        
df.tail()


## Insert
query = text("""
    INSERT INTO mention (dt, stock_id, message, source, url)
    VALUES (%s, %s, %s, 'wallstreetbets', %s)
    """, (submitted_time, stocks[cashtag], submission.title, submission.url))




# =============================================================================
# SQLite3
# =============================================================================
con = sqlite3.connect(f_path + '/data/stocks.db')
cur = con.cursor()

#db = cur.execute("""select * from SILJ limit 3""")
#for row in db:
#    print(row)

query = """
    CREATE TABLE COT_gold (
        date TEXT PRIMARY KEY,
        banks_netshort INTEGER
        )
    """
# Create table
cur.execute("""
    CREATE TABLE IF NOT EXISTS stocks (
        id INTEGER PRIMARY KEY,
        symbol TEXT NOT NULL UNIQUE, 
        name TEXT,
        exchange TEXT,
        currency TEXT,
        shares INTEGER
        )
    """)
# TEXT, SERIAL, INTEGER, NUMERIC, DATE, TIMESTAMP WITHOUT TIME ZONE
# FOREIGN KEY (sotck_id) REFERENCES stock (id)
# PRIMARY KEY (id, symbol)
    
# Insert a row of data
symbol = 'XAU/USD'
name = 'Gold'
cur.execute(f"""
    INSERT INTO stocks (symbol, name) 
    VALUES ('{symbol}', '{name}')
    """)


# Save (commit) the changes
con.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
con.close()


# Test
#db = cur.execute("""select * from COT_silver where date='2003'""")
##db = cur.execute("""delete from COT_silver where date='2003'""")
#for row in db: print(row)
#con.commit()

# =============================================================================
# TimescaleDB - PostgreSQL for Time-Series Data
# =============================================================================

import psycopg2