
"""
  https://stackoverflow.com/questions/24293895/how-to-use-python-logging-to-log-to-files
"""


import logging 
from functools import wraps


f_path = 'C:/frodriguezp/DataScience/Python'

# Example from: https://www.geeksforgeeks.org/create-an-exception-logging-decorator-in-python/

def create_logger(f_path): 

    new_run = "\n\n============= NEW RUN =============\n"
    fmt = new_run + '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt) 
    	
    # create a logger object 
    logger = logging.getLogger('exc_logger') 
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(f_path + '/exc_logger.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    
    # add the handlers to the logger
    logger.addHandler(fh)
    	
    return logger 

logger = create_logger(f_path) 

def log_exception(logger):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                # message to be displayed in formatter
                issue = "exception in "+func.__name__+"\n"
                logger.exception(issue)
                raise
        return wrapper
    return decorator 


@log_exception(logger) 
def test_fail(x):
#    raise Exception ('test exception')
    raise custom_error(55,'aaa',44)
    a=[1,2,3]
    a[5]
    return x**2


# Driver Code 
if __name__ == '__main__': 
	test_fail(5) 

# el decorator solo esta en la funcion

    
    
    