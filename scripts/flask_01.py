"""
Created on Mon Nov 29 10:03:13 2021

@author: ESRodrigFr1

Sources:
 - Flask, to create a webpage (==Django)
     https://www.youtube.com/watch?v=mqhxxeeTbu0&list=PLzMcBGfZo4-n4vJJybUVV3Un_NFS5EOgX&index=2
 - Dash: flask + react.js + plotly.js
     https://realpython.com/python-dash/
     https://towardsdatascience.com/plotly-dashboards-in-python-28a3bb83702c
 - Streamlit, easy to use. Compatible with plotly and bokeh
     https://streamlit.io/
     https://docs.streamlit.io/library/api-reference
     Port: https://discuss.streamlit.io/t/deploying-on-rpi-with-my-own-domain/935
 - Interactive visualization: plotly, Bokeh, Altair
 - Classical visualization: matplotlib, seaborn
 - Winner: Streamlit
     https://www.star-history.com/#streamlit/streamlit&plotly/dash
 - Financial Candlesticl chart
     Brokeh: https://docs.bokeh.org/en/latest/docs/gallery/candlestick.html
     Comparison all: https://coderzcolumn.com/tutorials/data-science/candlestick-chart-in-python-mplfinance-plotly-bokeh

"""

if 0:
    from flask import Flask
    
    app = Flask(__name__)
    
    # Defining the home page of our site
    @app.route("/")  # this sets the route to this page
    def home():
    	return "Hello! this is the main page <h1>HELLO</h1>"  # some basic inline html
    
    
    @app.route("/<name>")
    def user(name):
        return f"Hello {name}!"
    
    if __name__ == "__main__":
        app.run()
    

if 0:
    import dash
    import dash_core_components as dcc
    import dash_html_components as html
    import plotly.express as px
    import pandas as pd
    
    app = dash.Dash()
    
    df = pd.read_csv(
        "https://raw.githubusercontent.com/ThuwarakeshM/geting-started-with-plottly-dash/main/life_expectancy.csv"
    )
    
    fig = px.scatter(
        df,
        x="GDP",
        y="Life expectancy",
        size="Population",
        color="continent",
        hover_name="Country",
        log_x=True,
        size_max=60,
    )
    
    app.layout = html.Div([dcc.Graph(id="life-exp-vs-gdp", figure=fig)])
    
    
    if __name__ == "__main__":
        app.run_server(debug=True)
        
# only woks with chrome
import os
import random
import requests
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from datetime import date, datetime, timedelta
import time

from plotly.offline import download_plotlyjs, init_notebook_mode,  plot
import plotly.graph_objects as go
#from alpha_vantage.timeseries import TimeSeries

import mplfinance as mpf
#import yfinance as yf

import streamlit as st


f_path = "C:/frodriguezp/DataScience/Investing/"
os.chdir(f_path + "Scripts/")

## Side bar
stocki = st.sidebar.selectbox(
     'Choose stock',
     ('Abrapalata', 'First Majestic', 'Wheaton'))

print(stocki)


## Import holdings
#portfolio_options = ['GDXJ','SILJ','TSM']
@st.cache
def load_holdings():
    holdings = pd.read_csv(f_path + 'data/holdings.csv', delimiter=';')  
    #holdings_options = pd.read_csv(f_path + 'data/holdings_options.csv', delimiter=';')  
    return holdings

st.write("""
         # Financial Dashboard
         Stock list
""")

dt = load_holdings()
st.dataframe(dt)

## Individual stock
st.write('## Individual stock')
st.write('You selected:', stocki)
    
    
    
